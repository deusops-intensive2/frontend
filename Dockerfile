FROM node:16.19-alpine3.16 as build
WORKDIR /app
COPY . .
RUN npm install
RUN npm run build

FROM node:16.19-alpine3.16
RUN npm install -g serve
WORKDIR /app
COPY --from=build /app/build /app/build
EXPOSE 3000
ENV NODE_ENV production
CMD [ "npx", "serve", "build" ]